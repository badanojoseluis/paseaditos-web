import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    dogs: [],
    walks: [],
    walksReport: [],
  },
  getters: {
    getDogs: (state) => state.dogs,
    getWalks: (state) => state.walks,
    getWalksReport: (state) => state.walksReport,
  },
  mutations: {
    SET_DOGS(state, dogs) {
      state.dogs = dogs;
    },
    SET_WALKS(state, walks) {
      state.walks = walks;
    },
    SET_WALKS_REPORT(state, walksReport) {
      state.walksReport = walksReport;
    },
  },
  actions: {
    async fetchDogs({ commit }) {
      try {
        const data = await axios.get(
          "https://paseaditos-py.herokuapp.com/dogs/"
        );
        commit("SET_DOGS", data.data);
      } catch (error) {
        alert(error);
        console.log(error);
      }
    },
    async addDog({ dispatch }, dog) {
      try {
        await axios.post("https://paseaditos-py.herokuapp.com/dogs/", dog);
        dispatch("fetchDogs");
      } catch (error) {
        alert(error);
        console.log(error);
      }
    },
    async fetchWalks({ commit }) {
      try {
        const data = await axios.get(
          "https://paseaditos-py.herokuapp.com/dogWalk/"
        );
        commit("SET_WALKS", data.data);
      } catch (error) {
        alert(error);
        console.log(error);
      }
    },
    async addWalk({ dispatch }, dog) {
      try {
        await axios.post("https://paseaditos-py.herokuapp.com/dogWalk/", dog);
        dispatch("fetchWalks");
      } catch (error) {
        alert(error);
        console.log(error);
      }
    },
    async fetchWalksReport({ commit }) {
      try {
        const data = await axios.get(
          "https://paseaditos-py.herokuapp.com/dogWalk/report/"
        );
        commit("SET_WALKS_REPORT", data.data);
      } catch (error) {
        alert(error);
        console.log(error);
      }
    },
  },
  modules: {},
});
