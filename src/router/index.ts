import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import HomeView from "../views/DogsView.vue";
import WalksView from "../views/WalksView.vue";
import WalksReportView from "../views/WalksReportView.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/walks",
    name: "walks",
    component: WalksView,
  },
  {
    path: "/walksReport",
    name: "walksReport",
    component: WalksReportView,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
