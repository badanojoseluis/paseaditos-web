# paseaditos-web

Este repo incluye el código de la aplicación web de paseaditos.
Para la web se utilizo el framework de Vue con:

* Router para la navegacion
* Vuex para el manejo de estados
* Axios para el manejo de peticiones HTTP
* Bootstrap-Vue para el manejo de componentes

NOTA: Para correr este proyecto se debe estar ejecutando el backend en el puerto `8000`

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
